###### [https://gitlab.com/FadeevAlexey/sp-04](https://gitlab.com/FadeevAlexey/sp-04)
# Task Manager 1.1.4

A simple web task manager, can help you organize your tasks.

### Built with
  - Java 8
  - Maven 4.0
  - Spring web MVC 5.0.8
  - Hibernate 5.4
  - Tomcat 7.0.47
  
### Developer
Alexey Fadeev
[alexey.v.fadeev@gmail.com](mailto:alexey.v.fadeev@gmail.com?subject=TaskManager)

### Building from source

```sh
$ git clone http://gitlab.volnenko.school/FadeevAlexey/sp-04.git
$ cd sp-04
$ mvn clean
$ mvn install
```

### Running server

```sh
$ cd tm-server
$ mvn tomcat7:run
```

### Running client

```sh
$ cd tm-client
$ mvn tomcat7:run
```