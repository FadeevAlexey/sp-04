package ru.fadeev.tm.client;

import feign.Feign;
import feign.Headers;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.*;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.fadeev.tm.dto.ProjectDTO;

import java.util.List;

@FeignClient
@Headers("Content-Type: application/json")
public interface ProjectClient {

    static ProjectClient client(@NotNull final String baseUrl) {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(ProjectClient.class, baseUrl);
    }

    @NotNull
    @GetMapping("/project")
    List<ProjectDTO> findAll();

    @PostMapping("/project")
    ProjectDTO createProject(@NotNull ProjectDTO projectDTO);

    @GetMapping("/project/{id}")
    ProjectDTO getProject(@PathVariable(name = "id") @NotNull String id);

    @DeleteMapping("/project/{id}")
    void deleteProject(@PathVariable(name = "id") @NotNull String id);

    @PutMapping("/project")
    ProjectDTO updateProject(final ProjectDTO projectDTO);

}