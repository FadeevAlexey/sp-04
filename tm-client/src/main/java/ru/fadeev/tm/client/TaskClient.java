package ru.fadeev.tm.client;

import feign.Feign;
import feign.Headers;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.fadeev.tm.dto.TaskDTO;

import java.util.List;

@FeignClient
@Headers("Content-Type: application/json")
public interface TaskClient {

    static TaskClient client(@NotNull final String baseUrl) {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(TaskClient.class, baseUrl);
    }

    @NotNull
    @GetMapping("/task")
    List<TaskDTO> findAll();

    @PostMapping("/task")
    TaskDTO createTask(@NotNull TaskDTO taskDTO);

    @GetMapping("/task/{id}")
    TaskDTO getTask(@PathVariable(name = "id") @NotNull String id);

    @DeleteMapping("/task/{id}")
    void deleteTask(@PathVariable(name = "id") @NotNull String id);

    @PutMapping("/task")
    TaskDTO updateTask(final TaskDTO taskDTO);

}