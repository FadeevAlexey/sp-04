package ru.fadeev.tm.config;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.fadeev.tm.client.ProjectClient;
import ru.fadeev.tm.client.TaskClient;

@Configuration
public class AppConfig {

    @NotNull
    @Value("${server.url}")
    private String serverUrl;

    @Bean
    public ProjectClient projectClient() {
        return ProjectClient.client(serverUrl);
    }

    @Bean
    public TaskClient taskClient() {
        return TaskClient.client(serverUrl);
    }

}
