package ru.fadeev.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.fadeev.tm.client.ProjectClient;
import ru.fadeev.tm.dto.ProjectDTO;
import ru.fadeev.tm.enumerated.Status;

import java.util.Date;

@Controller
public class ProjectController {

    @Autowired
    ProjectClient projectClient;

    @GetMapping(value = "/project")
    public ModelAndView projectListGet() {
        @NotNull final ModelAndView model = new ModelAndView("project_list");
        model.addObject("projects", projectClient.findAll());
        model.setStatus(HttpStatus.OK);
        return model;
    }

    @GetMapping(value = "/project/create")
    public ModelAndView projectCreateGet() {
        @NotNull final ModelAndView model = new ModelAndView("project_create");
        model.setStatus(HttpStatus.OK);
        return model;
    }

    @PostMapping(value = "/project/create")
    public ModelAndView projectCreatePost(
            @RequestParam @NotNull final String name,
            @RequestParam @Nullable final String description,
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") @Nullable final Date startDate,
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") @Nullable final Date finishDate,
            @RequestParam @NotNull final Status status
    ) {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName(name);
        project.setDescription(description);
        project.setStartDate(startDate);
        project.setFinishDate(finishDate);
        project.setStatus(status);
        projectClient.createProject(project);
        @NotNull final ModelAndView modelAndView = new ModelAndView("redirect:/project");
        modelAndView.setStatus(HttpStatus.MOVED_PERMANENTLY);
        return modelAndView;
    }

    @GetMapping(value = "/project/remove/{id}")
    public ModelAndView projectRemoveGet(@PathVariable @NotNull final String id) {
        projectClient.deleteProject(id);
        @NotNull final ModelAndView modelAndView = new ModelAndView("redirect:/project");
        modelAndView.setStatus(HttpStatus.MOVED_PERMANENTLY);
        return modelAndView;
    }

    @GetMapping(value = "/project/edit/{id}")
    public ModelAndView projectEditGet(@PathVariable @NotNull final String id) {
        @NotNull final ModelAndView model = new ModelAndView("project_edit");
        model.addObject("project", projectClient.getProject(id));
        model.setStatus(HttpStatus.OK);
        return model;
    }

    @GetMapping(value = "/project/view/{id}")
    public ModelAndView projectViewGet(@PathVariable @NotNull final String id) {
        @NotNull final ModelAndView model = new ModelAndView("project_view");
        @NotNull final ProjectDTO project = projectClient.getProject(id);
        model.addObject("project", project);
        model.setStatus(HttpStatus.OK);
        return model;
    }

    @PostMapping(value = "/project/edit/{id}")
    public ModelAndView projectEditPost(
            @PathVariable @Nullable final String id,
            @RequestParam @Nullable final String name,
            @RequestParam @Nullable final String description,
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") @Nullable final Date startDate,
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") @Nullable final Date finishDate,
            @RequestParam @Nullable final Status status
    ) {
        @NotNull ModelAndView model = new ModelAndView("redirect:/project");
        model.setStatus(HttpStatus.BAD_REQUEST);
        if (id == null) return model;
        @Nullable final ProjectDTO project = projectClient.getProject(id);
        if (project == null) return model;
        project.setId(id);
        if (name != null && !name.isEmpty()) project.setName(name);
        if (description != null && !description.isEmpty()) project.setDescription(description);
        project.setStartDate(startDate);
        project.setFinishDate(finishDate);
        if (status != null) project.setStatus(status);
        projectClient.updateProject(project);
        model.setStatus(HttpStatus.OK);
        return model;
    }

}