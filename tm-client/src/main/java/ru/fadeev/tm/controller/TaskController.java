package ru.fadeev.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ru.fadeev.tm.client.ProjectClient;
import ru.fadeev.tm.client.TaskClient;
import ru.fadeev.tm.dto.TaskDTO;
import ru.fadeev.tm.enumerated.Status;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class TaskController {

    @NotNull
    @Autowired
    TaskClient taskClient;

    @NotNull
    @Autowired
    ProjectClient projectClient;

    @GetMapping(value = "/task")
    public ModelAndView taskListGet() {
        @NotNull final ModelAndView model = new ModelAndView("task_list");
        model.addObject("tasks", taskClient.findAll());
        model.setStatus(HttpStatus.OK);
        return model;
    }

    @GetMapping(value = "/task/create")
    public ModelAndView taskCreateGet() {
        @NotNull final List<String> projects = new ArrayList<>();
        projectClient.findAll().forEach(project -> projects.add(project.getName()));
        @NotNull final ModelAndView model = new ModelAndView("task_create");
        model.addObject("projects", projects);
        model.setStatus(HttpStatus.OK);
        return model;
    }


    @PostMapping(value = "/task/create")
    public ModelAndView taskCreatePost(
            @RequestParam @NotNull final String project,
            @RequestParam @NotNull final String name,
            @RequestParam @Nullable final String description,
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") @Nullable final Date startDate,
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") @Nullable final Date finishDate,
            @RequestParam @NotNull final Status status
    ) {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName(name);
        task.setDescription(description);
        task.setStartDate(startDate);
        task.setFinishDate(finishDate);
        task.setStatus(status);
        task.setProjectName(project);
        taskClient.createTask(task);
        @NotNull final ModelAndView modelAndView = new ModelAndView("redirect:/task");
        modelAndView.setStatus(HttpStatus.MOVED_PERMANENTLY);
        System.out.println("x");
        return modelAndView;
    }

    @GetMapping(value = "/task/remove/{id}")
    public ModelAndView TaskRemoveGet(@PathVariable @NotNull final String id) {
        taskClient.deleteTask(id);
        @NotNull final ModelAndView modelAndView = new ModelAndView("redirect:/task");
        modelAndView.setStatus(HttpStatus.MOVED_PERMANENTLY);
        return modelAndView;
    }

    @GetMapping(value = "/task/view/{id}")
    public ModelAndView taskViewGet(@PathVariable @NotNull final String id) {
        @NotNull final ModelAndView model = new ModelAndView("task_view");
        model.addObject("task", taskClient.getTask(id));
        model.setStatus(HttpStatus.OK);
        return model;
    }

    @GetMapping(value = "/task/edit/{id}")
    public ModelAndView taskEditGet(@PathVariable @NotNull final String id) {
        @NotNull final List<String> projects = new ArrayList<>();
        projectClient.findAll().forEach(project -> projects.add(project.getName()));
        @NotNull final TaskDTO task = taskClient.getTask(id);
        @NotNull final ModelAndView model = new ModelAndView("task_edit");
        model.addObject("projects", projects);
        model.setStatus(HttpStatus.OK);
        model.addObject("task", task);
        model.addObject("projects", projects);
        model.setStatus(HttpStatus.OK);
        return model;
    }

    @PostMapping(value = "/task/edit/{id}")
    public ModelAndView taskEditPost(
            @PathVariable @Nullable String id,
            @RequestParam @NotNull final String project,
            @RequestParam @Nullable final String name,
            @RequestParam @Nullable final String description,
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") @Nullable final Date startDate,
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") @Nullable final Date finishDate,
            @RequestParam @Nullable final Status status
    ) {
        @NotNull ModelAndView model = new ModelAndView("redirect:/task");
        model.setStatus(HttpStatus.BAD_REQUEST);
        if (id == null) return model;
        @Nullable final TaskDTO task = taskClient.getTask(id);
        if (task == null) return model;
        task.setProjectName(project);
        if (name != null && !name.isEmpty()) task.setName(name);
        if (description != null && !description.isEmpty()) task.setDescription(description);
        task.setStartDate(startDate);
        task.setFinishDate(finishDate);
        if (status != null) task.setStatus(status);
        taskClient.updateTask(task);
        model.setStatus(HttpStatus.OK);
        return model;
    }

}