package ru.fadeev.tm.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@ToString(callSuper=true)
public final class TaskDTO extends AbstractEntityDTO {

    private static final long serialVersionUID = 2523285273276658030L;

    @NotNull
    private String name = "";

    @Nullable
    private String description = "";

    @Nullable
    private String projectId;

    @Nullable
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private Date startDate;

    @Nullable
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private Date finishDate;

    @Nullable
    private String userId;

    @NotNull
    private Status status = Status.PLANNED;

    @Nullable
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private Date creationTime = new Date(System.currentTimeMillis());

    @Nullable
    private String projectName;

}