package ru.fadeev.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.enumerated.Role;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@ToString(callSuper=true)
public final class UserDTO extends AbstractEntityDTO implements Serializable {

    private static final long serialVersionUID = -7402071995321582277L;

    @Nullable
    private String login ="";

    @Nullable
    private String passwordHash = "";

    @NotNull
    private Role role = Role.USER;

}