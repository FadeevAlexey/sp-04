package ru.fadeev.tm;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.junit.experimental.categories.Category;
import ru.fadeev.tm.client.ProjectClient;
import ru.fadeev.tm.client.TaskClient;

@Getter
@Setter
@Category(ru.fadeev.tm.IntegrationTest.class)
public abstract class AbstractTest {

    @NotNull final String serverUrl = "http://localhost:8080";

    @NotNull final ProjectClient projectClient = ProjectClient.client(serverUrl);

    @NotNull final TaskClient taskClient = TaskClient.client(serverUrl);

}