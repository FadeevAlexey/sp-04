package ru.fadeev.tm;

import feign.FeignException;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.fadeev.tm.dto.ProjectDTO;

@Category(IntegrationTest.class)
public class ProjectRestApiTest extends AbstractTest {

    @Test
    public void persistProjectTest() {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("testUser");
        projectClient.createProject(project);
        @NotNull final ProjectDTO projectResult = projectClient.getProject(project.getId());
        Assert.assertEquals("testUser", projectResult.getName());
        projectClient.deleteProject(project.getId());
    }

    @Test
    public void findAllProjectTest() {
        final int startTest = projectClient.findAll().size();
        @NotNull ProjectDTO project = new ProjectDTO();
        @NotNull ProjectDTO project2 = new ProjectDTO();
        @NotNull ProjectDTO project3 = new ProjectDTO();
        projectClient.createProject(project);
        projectClient.createProject(project2);
        projectClient.createProject(project3);
        final int finishTest = projectClient.findAll().size();
        Assert.assertEquals(startTest, finishTest - 3);
        Assert.assertNotNull(projectClient.getProject(project.getId()));
        projectClient.deleteProject(project.getId());
        projectClient.deleteProject(project2.getId());
        projectClient.deleteProject(project3.getId());
    }

    @Test
    public void getOneProjectTest() {
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setName("test");
        projectClient.createProject(project);
        @NotNull final ProjectDTO testProject = projectClient.getProject(project.getId());
        Assert.assertEquals(project.getName(), testProject.getName());
        projectClient.deleteProject(project.getId());
    }

    @Test
    public void mergeProjectTest() {
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setName("test");
        projectClient.createProject(project);
        @NotNull ProjectDTO testProject = projectClient.getProject(project.getId());
        testProject.setDescription("test description");
        projectClient.updateProject(testProject);
        testProject = projectClient.getProject(testProject.getId());
        Assert.assertEquals("test description", testProject.getDescription());
        projectClient.deleteProject(project.getId());
    }

    @Test
    public void removeProjectTest() {
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setName("test");
        projectClient.createProject(project);
        Assert.assertEquals("test", projectClient.getProject(project.getId()).getName());
        projectClient.deleteProject(project.getId());
        try {
            projectClient.getProject(project.getId());
        } catch (FeignException e) {
            Assert.assertTrue(e.getMessage().contains("status 404"));
        }
    }

}
