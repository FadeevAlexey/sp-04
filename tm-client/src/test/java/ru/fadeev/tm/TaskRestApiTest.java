package ru.fadeev.tm;

import feign.FeignException;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.fadeev.tm.dto.TaskDTO;

@Category(IntegrationTest.class)
public class TaskRestApiTest extends AbstractTest {

    @Test
    public void persistTaskTest() {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName("testUser");
        taskClient.createTask(task);
        @NotNull final TaskDTO taskResult = taskClient.getTask(task.getId());
        Assert.assertEquals("testUser", taskResult.getName());
        taskClient.deleteTask(task.getId());
    }

    @Test
    public void findAllTaskTest() {
        final int startTest = taskClient.findAll().size();
        @NotNull TaskDTO task = new TaskDTO();
        @NotNull TaskDTO task2 = new TaskDTO();
        @NotNull TaskDTO task3 = new TaskDTO();
        taskClient.createTask(task);
        taskClient.createTask(task2);
        taskClient.createTask(task3);
        final int finishTest = taskClient.findAll().size();
        Assert.assertEquals(startTest, finishTest - 3);
        Assert.assertNotNull(taskClient.getTask(task.getId()));
        taskClient.deleteTask(task.getId());
        taskClient.deleteTask(task2.getId());
        taskClient.deleteTask(task3.getId());
    }

    @Test
    public void getOneTaskTest() {
        @NotNull TaskDTO task = new TaskDTO();
        task.setName("test");
        taskClient.createTask(task);
        @NotNull final TaskDTO testTask = taskClient.getTask(task.getId());
        Assert.assertEquals(task.getName(), testTask.getName());
        taskClient.deleteTask(task.getId());
    }

    @Test
    public void mergeTaskTest() {
        @NotNull TaskDTO task = new TaskDTO();
        task.setName("test");
        taskClient.createTask(task);
        @NotNull TaskDTO testTask = taskClient.getTask(task.getId());
        testTask.setDescription("test description");
        taskClient.updateTask(testTask);
        testTask = taskClient.getTask(testTask.getId());
        Assert.assertEquals("test description", testTask.getDescription());
        taskClient.deleteTask(task.getId());
    }

    @Test
    public void removeTaskTest() {
        @NotNull TaskDTO task = new TaskDTO();
        task.setName("test");
        taskClient.createTask(task);
        Assert.assertEquals("test", taskClient.getTask(task.getId()).getName());
        taskClient.deleteTask(task.getId());
        try {
            taskClient.getTask(task.getId());
        } catch (FeignException e) {
            Assert.assertTrue(e.getMessage().contains("status 404"));
        }
    }

}
