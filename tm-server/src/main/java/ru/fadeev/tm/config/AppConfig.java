package ru.fadeev.tm.config;

import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@PropertySource("classpath:config.properties")
@EnableJpaRepositories("ru.fadeev.tm.api.repository")
public class AppConfig {

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            @Value("${hibernate.show.sql}") final String showSql,
            @Value("${hibernate.hbm2ddl.auto}") final String hbm2ddl,
            @Value("${hibernate.dialect}") final String dialect,
            @Value("${hibernate.use.second.level.cache}") final String secondLevelCache,
            @Value("${hibernate.use.query.cache}") final String useQueryCache,
            @Value("${hibernate.use.minimal.puts}") final String useMinimalPuts,
            @Value("${hibernate.cache.hazelcast.use_lite_member}") final String useLiteMember,
            @Value("${hibernate.cache.region.prefix}") final String regionPrefix,
            @Value("${hibernate.cache.region.factory}") final String regionFactory,
            @Value("${hibernate.cache.provider.config}") final String providerConfig,
            final DataSource dataSource
    ) {
        final LocalContainerEntityManagerFactoryBean factoryBean;
        factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.fadeev.tm.entity");
        final Properties properties = new Properties();
        properties.put(Environment.SHOW_SQL, showSql);
        properties.put(Environment.HBM2DDL_AUTO,hbm2ddl);
        properties.put(Environment.DIALECT, dialect);
        properties.put(Environment.USE_SECOND_LEVEL_CACHE, secondLevelCache);
        properties.put(Environment.USE_QUERY_CACHE, useQueryCache);
        properties.put(Environment.USE_MINIMAL_PUTS, useMinimalPuts);
        properties.put("hibernate.cache.hazelcast.use_lite_member", useLiteMember);
        properties.put(Environment.CACHE_REGION_PREFIX, regionPrefix);
        properties.put(Environment.CACHE_REGION_FACTORY, regionFactory);
        properties.put(Environment.CACHE_PROVIDER_CONFIG, providerConfig);
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

    @Bean
    public PlatformTransactionManager transactionManager(
            final LocalContainerEntityManagerFactoryBean entityManagerFactory
    ) {
        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

    @Bean
    @NotNull
    public DataSource dataSource(
            @Value("${db.driver}") final String dataSourceDriver,
            @Value("${db.url}") final String dataSourceUrl,
            @Value("${db.login}") final String dataSourceUser,
            @Value("${db.password}") final String dataSourcePassword
    ) {
        @NotNull final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(dataSourceDriver);
        dataSource.setUrl(dataSourceUrl);
        dataSource.setUsername(dataSourceUser);
        dataSource.setPassword(dataSourcePassword);
        return dataSource;
    }

}