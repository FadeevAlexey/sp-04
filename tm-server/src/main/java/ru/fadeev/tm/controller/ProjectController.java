package ru.fadeev.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.fadeev.tm.api.service.IProjectService;
import ru.fadeev.tm.dto.ProjectDTO;
import ru.fadeev.tm.entity.Project;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class ProjectController {

    @NotNull
    @Autowired
    private IProjectService projectService;

    @GetMapping(value = "/project")
    public ResponseEntity<List<ProjectDTO>> projectListGet() {
        @NotNull final List<ProjectDTO> projects = projectService.findAll().stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());
        return ResponseEntity.ok(projects);
    }

    @PostMapping(value = "/project")
    public ResponseEntity<ProjectDTO> projectCreatePost(@RequestBody @Nullable final ProjectDTO projectDTO) {
        if (projectDTO == null) return ResponseEntity.badRequest().build();
        @NotNull final Project project = convertToProject(projectDTO);
        projectService.persist(project);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(value = "/project/{id}")
    public ResponseEntity<ProjectDTO> projectRemoveGet(@PathVariable @Nullable final String id) {
        if (id == null || id.isEmpty()) return ResponseEntity.badRequest().build();
        projectService.remove(id);
        return ResponseEntity.ok().build();
    }

    @PutMapping(value = "/project")
    public ResponseEntity<ProjectDTO> projectEditPost(@RequestBody @Nullable ProjectDTO projectDTO) {
        if (projectDTO == null) return ResponseEntity.badRequest().build();
        projectService.merge(convertToProject(projectDTO));
        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "/project/{id}")
    public ResponseEntity<ProjectDTO> projectViewGet(@PathVariable @NotNull final String id) {
        @Nullable final Project project = projectService.findOne(id);
        if (project == null) return ResponseEntity.notFound().build();
        return ResponseEntity.ok(convertToDTO(project));
    }

    @NotNull
    private Project convertToProject(@NotNull final ProjectDTO projectDTO) {
        @Nullable Project project = projectService.findOne(projectDTO.getId());
        if (project == null) project = new Project();
        project.setId(projectDTO.getId());
        project.setName(projectDTO.getName());
        project.setDescription(projectDTO.getDescription());
        project.setStartDate(projectDTO.getStartDate());
        project.setFinishDate(projectDTO.getFinishDate());
        project.setStatus(projectDTO.getStatus());
        project.setCreationTime(projectDTO.getCreationTime());
        return project;
    }

    @NotNull
    private ProjectDTO convertToDTO(@NotNull final Project project) {
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setId(project.getId());
        projectDTO.setName(project.getName());
        projectDTO.setDescription(project.getDescription());
        projectDTO.setStartDate(project.getStartDate());
        projectDTO.setFinishDate(project.getFinishDate());
        if (project.getUser() != null) projectDTO.setUserId(project.getUser().getId());
        projectDTO.setStatus(project.getStatus());
        projectDTO.setCreationTime(project.getCreationTime());
        return projectDTO;
    }

}