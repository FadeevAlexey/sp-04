package ru.fadeev.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.fadeev.tm.api.service.IProjectService;
import ru.fadeev.tm.api.service.ITaskService;
import ru.fadeev.tm.dto.ProjectDTO;
import ru.fadeev.tm.dto.TaskDTO;
import ru.fadeev.tm.entity.Task;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class TaskController {

    @NotNull
    @Autowired
    private ITaskService taskService;

    @NotNull
    @Autowired
    private IProjectService projectService;

    @GetMapping(value = "/task")
    public ResponseEntity<List<TaskDTO>> taskListGet() {
        @NotNull final List<TaskDTO> tasks = taskService.findAll().stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());
        return ResponseEntity.ok(tasks);
    }

    @PostMapping(value = "/task")
    public ResponseEntity<TaskDTO> taskCreatePost(@RequestBody @NotNull final TaskDTO taskDTO) {
        taskService.persist(convertToTask(taskDTO));
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(value = "/task/{id}")
    public ResponseEntity<ProjectDTO> TaskRemoveGet(@PathVariable @NotNull final String id) {
        taskService.remove(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "/task/{id}")
    public ResponseEntity<TaskDTO> taskViewGet(@PathVariable @NotNull final String id) {
        @Nullable final Task task = taskService.findOne(id);
        if (task == null) return ResponseEntity.notFound().build();
        return ResponseEntity.ok(convertToDTO(task));
    }

    @PutMapping(value = "/task")
    public ResponseEntity<TaskDTO> taskEditPost(@RequestBody @Nullable TaskDTO taskDTO) {
        if (taskDTO == null) return ResponseEntity.badRequest().build();
        taskService.merge(convertToTask(taskDTO));
        return ResponseEntity.ok().build();
    }

    @NotNull
    private TaskDTO convertToDTO(@NotNull final Task task) {
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setId(task.getId());
        taskDTO.setName(task.getName());
        taskDTO.setDescription(task.getDescription());
        if (task.getProject() != null) taskDTO.setProjectId(task.getProject().getId());
        taskDTO.setStartDate(task.getStartDate());
        taskDTO.setFinishDate(task.getFinishDate());
        if (task.getUser() != null) taskDTO.setUserId(task.getUser().getId());
        taskDTO.setStatus(task.getStatus());
        taskDTO.setCreationTime(task.getCreationTime());
        if (task.getProject() != null) taskDTO.setProjectName(task.getProject().getName());
        return taskDTO;
    }

    @NotNull
    private Task convertToTask(@NotNull final TaskDTO taskDTO) {
        @NotNull Task task = new Task();
        task.setId(taskDTO.getId());
        task.setName(taskDTO.getName());
        task.setDescription(taskDTO.getDescription());
        final String projectId = projectService.findIdByName(taskDTO.getProjectName());
        task.setProject(projectService.findOne(projectId));
        task.setStartDate(taskDTO.getStartDate());
        task.setFinishDate(taskDTO.getFinishDate());
        task.setStatus(taskDTO.getStatus());
        task.setCreationTime(taskDTO.getCreationTime());
        return task;
    }

}