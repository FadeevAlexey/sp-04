package ru.fadeev.tm.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

@Getter
@Setter
@ToString
public abstract class AbstractEntityDTO {

    @NotNull
    private String id = UUID.randomUUID().toString();

    @NotNull
    public String getId() {
        return id;
    }

}