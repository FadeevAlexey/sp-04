package ru.fadeev.tm.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.enumerated.Status;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@ToString(callSuper=true)
public final class ProjectDTO extends AbstractEntityDTO implements Serializable {

    private static final long serialVersionUID = 5371138000748416222L;

    @NotNull
    private String name = "";

    @Nullable
    private String description = "";

    @Nullable
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private Date startDate;

    @Nullable
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private Date finishDate;

    @Nullable
    private String userId;

    @NotNull
    private Status status = Status.PLANNED;

    @NotNull
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private Date creationTime = new Date(System.currentTimeMillis());

}