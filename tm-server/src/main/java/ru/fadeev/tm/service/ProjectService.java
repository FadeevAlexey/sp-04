package ru.fadeev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.fadeev.tm.api.repository.IProjectRepository;
import ru.fadeev.tm.api.service.IProjectService;
import ru.fadeev.tm.entity.Project;
import ru.fadeev.tm.specification.Specifications;

import java.util.*;

@Service
@Transactional
public class ProjectService extends AbstractService<Project> implements IProjectService {

    @NotNull
    @Autowired
    private IProjectRepository projectRepository;

    @NotNull
    @Override
    public List<Project> findAll() {
       return projectRepository.findAll();
    }

    @Nullable
    @Override
    public Project findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.findById(id).orElse(null);
    }

    @Nullable
    @Override
    public Project findOne(@Nullable final String userId, final String id) {
        if (userId == null || userId.isEmpty()) return null;
        return projectRepository.findOne(Specifications.findOne(userId, id)).orElse(null);
    }

    @Override
    public void remove(@Nullable final String id) {
        if (id == null || id.isEmpty()) return;
        projectRepository.deleteById(id);
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) return;
        if (projectId == null || projectId.isEmpty()) return;
        projectRepository.removeByUser_IdAndId(userId, projectId);
    }

    @Override
    public void persist(@Nullable final Project project) {
        if (project == null) return;
        projectRepository.save(project);
    }

    @Override
    public void merge(@Nullable final Project project) {
        if (project == null) return;
        projectRepository.save(project);
    }

    @Override
    public void removeAll() {
        projectRepository.deleteAllInBatch();
    }

    @Nullable
    @Override
    public String findIdByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        @Nullable final Project project =
                projectRepository.findOne(Specifications.findIdByName(userId, name)).orElse(null);
        return project == null ? null : project.getId();
    }

    @Nullable
    @Override
    public String findIdByName(@Nullable final String name) {
        if (name == null || name.isEmpty()) return null;
        @Nullable final Project project =
                projectRepository.findOne(Specifications.findIdByName(name)).orElse(null);
        return project == null ? null : project.getId();
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return new ArrayList<>();
        return projectRepository.findAll(Specifications.findAllByUserId(userId));
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        projectRepository.removeAllByUser_Id(userId);
    }

    @NotNull
    public Collection<Project> sortByStartDate(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        return projectRepository.findAll(
                Specifications.findAllByUserId(userId),
                Sort.by(Sort.Direction.ASC, "startDate")
        );
    }

    @NotNull
    public Collection<Project> sortByFinishDate(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        return projectRepository.findAll(
                Specifications.findAllByUserId(userId),
                Sort.by(Sort.Direction.ASC, "finishDate")
        );
    }

    @NotNull
    public Collection<Project> sortByStatus(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        return projectRepository.findAll(
                Specifications.findAllByUserId(userId),
                Sort.by(Sort.Direction.ASC, "status")
        );
    }

    @NotNull
    public Collection<Project> sortByCreationDate(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        return projectRepository.findAll(
                Specifications.findAllByUserId(userId),
                Sort.by(Sort.Direction.ASC, "creationTime")
        );
    }

    @Override
    @NotNull
    public Collection<Project> searchByName(@Nullable final String userId, @Nullable final String string) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (string == null || string.isEmpty()) return Collections.emptyList();
        return projectRepository.findAll(Specifications.searchByName(userId, string));
    }

    @Override
    @NotNull
    public Collection<Project> searchByDescription(@Nullable final String userId, @Nullable final String string) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (string == null || string.isEmpty()) return Collections.emptyList();
        return projectRepository.findAll(Specifications.searchByDescription(userId, string));
    }

}