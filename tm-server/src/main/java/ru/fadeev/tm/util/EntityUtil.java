package ru.fadeev.tm.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import org.jetbrains.annotations.NotNull;

public class EntityUtil {

    public static String objectToJson(Object object) {
        try {
            @NotNull final SimpleFilterProvider filterProvider = new SimpleFilterProvider();
            filterProvider.addFilter("idFilter", SimpleBeanPropertyFilter.filterOutAllExcept("id"));
            @NotNull final ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.setFilterProvider(filterProvider);
            @NotNull final String json = objectMapper.writeValueAsString(object);
            return json;
        } catch (final JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

}